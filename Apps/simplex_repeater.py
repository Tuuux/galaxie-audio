#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Audio Team, all rights reserved

import sys
import tempfile


from glxaudio.AudioRecorder import AudioRecorder
from glxaudio.AudioPlayer import AudioPLayer
from glxaudio.AudioInterfaces import AudioInterfaces
from glxaudio.AudioConstants import GLXAUDIO
from glxaudio.Sleep import Sleep
from glxviewer import viewer

# THE APP
try:
    verbose = True
    debug = False
    debug_level = 3

    time_to_sleep = 0.42
    if verbose:
        viewer.write(
            status_text="INIT",
            status_text_color="WHITE",
            column_1="Simplex Repeater",
            column_2=" - Version 0.5",
        )

        viewer.write(
            status_text="INIT",
            status_text_color="WHITE",
            column_1=AudioInterfaces.__name__ + ":",
            column_2="list interfaces",
        )
        AudioInterfaces.print_interfaces(only_default=True)

    while True:
        #  Create a new temporary file each time, that because communication's should be anonyme
        temporary_file = tempfile.NamedTemporaryFile()
        try:
            # Start a recording
            with AudioRecorder() as recorder:
                recorder.debug = debug
                recorder.debug_level = debug_level
                recorder.verbose = verbose
                recorder.verbose_level = 3
                recorder.format = GLXAUDIO.FORMAT_INT16
                recorder.threshold = 6  # in percent
                recorder.channels = 1
                recorder.rate = 22050
                recorder.chunk_size = 1024
                recorder.file.path = temporary_file.name
                recorder.record_to_file()

            # Wait , because that is how work a repeater
            with Sleep() as sleeper:
                sleeper.debug = debug
                sleeper.debug_level = debug_level
                sleeper.verbose = verbose
                sleeper.sleep(time_to_sleep)

            # Play what is inside our temporary file
            with AudioPLayer() as player:
                player.debug = debug
                player.debug_level = debug_level
                player.verbose = verbose
                player.detached = True
                player.file.path = temporary_file.name
                player.play()

        except EOFError:
            pass

        # Close the temporary file, it have effect to delete the file, that because communication's should be anonymize
        temporary_file.close()

except KeyboardInterrupt:
    viewer.flush_a_new_line()
    sys.exit(0)
