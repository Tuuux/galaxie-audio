#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import sys
import os

from glxaudio.AudioSpectrogram import AudioSpectrogram

if __name__ == "__main__":
    # Do a basic screen clear.
    os.system("clear")
    # Turn the cursor off to look prettier... ;o)
    os.system("setterm -cursor off")

    with AudioSpectrogram() as spectrogram:
        try:
            while True:
                spectrogram.listen()

        except KeyboardInterrupt:
            spectrogram.stream_close()
            # Do a basic screen clear.
            os.system("clear")
            # Turn the cursor off to look prettier... ;o)
            os.system("setterm -cursor on")
            sys.exit(0)
