from glxaudio.Object import Object
import unittest


class TestObject(unittest.TestCase):

    def setUp(self):
        self.object = Object()

    def test_debug(self):
        self.assertFalse(self.object.debug)

        self.object.debug = True
        self.assertTrue(self.object.debug)

        self.assertRaises(TypeError, setattr, self.object, "debug", float(42))

    def test_debug_level(self):
        self.assertEqual(0, self.object.debug_level)

        self.object.debug_level = 3
        self.assertEqual(3, self.object.debug_level)

        # test error
        self.assertRaises(TypeError, setattr, self.object, 'debug_level', "Hello.42")

    def test_verbose(self):
        self.assertFalse(self.object.verbose)

        self.object.verbose = True
        self.assertTrue(self.object.verbose)

        self.assertRaises(TypeError, setattr, self.object, "verbose", "Hello.42")

    def test_verbose_level(self):
        self.assertEqual(0, self.object.verbose_level)

        self.object.verbose_level = 3
        self.assertEqual(3, self.object.verbose_level)

        self.assertRaises(TypeError, setattr, self.object, "verbose_level", "Hello.42")


if __name__ == '__main__':
    unittest.main()
