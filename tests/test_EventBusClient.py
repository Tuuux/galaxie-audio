import unittest
from glxaudio.EventBus import EventBusClient


class FakeApplication(object):
    def __init__(self, detailed_signal, *args):
        self.active_window_id = None
        self.main_window = None
        self.detailed_signal = None
        self.data = None

    def emit(self, detailed_signal, data):
        self.detailed_signal = detailed_signal
        self.data = data


class MyTestCase(unittest.TestCase):

    def test_set_get_application(self):
        with EventBusClient() as event_bus:
            fake_app1 = FakeApplication("42", "42")
            event_bus.set_application(fake_app1)
            self.assertEqual(event_bus.get_application(), fake_app1)

    def test_emit(self):
        with EventBusClient() as event_bus:
            fake_app1 = FakeApplication("42", "42")
            event_bus.set_application(fake_app1)
            self.assertEqual(event_bus.get_application(), fake_app1)
            detailed_signal = '42'
            data = {'Hello': 42}
            self.assertEqual(event_bus.get_application().detailed_signal, None)
            self.assertEqual(event_bus.get_application().data, None)
            event_bus.emit(detailed_signal, data)
            self.assertEqual(event_bus.get_application().detailed_signal, detailed_signal)
            self.assertEqual(event_bus.get_application().data, data)

    def test_connect(self):
        with EventBusClient() as event_bus:
            self.assertEqual(type(event_bus.get_events_list()), dict)
            self.assertRaises(TypeError, event_bus.connect, detailed_signal=42)
            self.assertRaises(TypeError, event_bus.connect, detailed_signal='Hello', handler=None, args=42)

            event_bus.connect(detailed_signal="Hello", handler=FakeApplication, args=[42, 42])
            # self.assertEqual(
            #     "{'Hello': [<class 'test.test_EventBusClient.FakeApplication'>, [42, 42]]}",
            #     str(event_bus.get_events_list())
            # )
            event_bus.connect(detailed_signal="Hello", handler=FakeApplication, args=[42, 42])
            event_bus.connect(detailed_signal="Hello42", handler=FakeApplication, args=[42, 42])
            # self.assertEqual(
            #     "{'Hello': [<class 'test.test_EventBusClient.FakeApplication'>, [42, 42]], 'Hello42': [<class "
            #     "'test.test_EventBusClient.FakeApplication'>, [42, 42]]}",
            #     str(event_bus.get_events_list())
            # )
            event_bus.disconnect(detailed_signal="Hello42", handler=FakeApplication)
            event_bus.disconnect(detailed_signal="Hello", handler=FakeApplication)

            event_bus.connect(detailed_signal="Hello42", handler=FakeApplication)
            # self.assertEqual(event_bus.get_events_list(), '')

    def test_disconnect(self):
        with EventBusClient() as event_bus:
            self.assertEqual(type(event_bus.get_events_list()), dict)
            handler = FakeApplication
            event_bus.connect(detailed_signal="Hello", handler=handler, args=[42, 42])
            # self.assertEqual(
            #     "{'Hello': [<class 'test.test_EventBusClient.FakeApplication'>, [42, 42]]}",
            #     str(event_bus.get_events_list())
            # )
            event_bus.disconnect(detailed_signal="Hello", handler=handler)

            # self.assertEqual(
            #     "{}",
            #     str(event_bus.get_events_list())
            # )

if __name__ == '__main__':
    unittest.main()
