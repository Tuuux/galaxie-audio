#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import unittest

from glxaudio.AudioConstants import Constants


# Unittest
class TestConstants(unittest.TestCase):
    def test_Constants_set(self):
        """Test Constants set"""
        const = Constants()
        const.hello = 42
        self.assertEqual(const.hello, 42)
        self.assertRaises(Constants.ConstError, const.__setattr__, 'hello', 42)

    def test_Constants_get(self):
        """Test Constants get"""
        const = Constants()
        const.hello = 42
        self.assertEqual(const.__getattr__('hello'), 42)
        self.assertRaises(Constants.ConstError, const.__getattr__, 'im_not')


if __name__ == '__main__':
    unittest.main()
