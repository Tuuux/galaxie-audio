#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

from glxaudio.AudioSWHear import AudioSWHear

import unittest
import pyaudio
import numpy
import os


# Unittest
class TestAudioSWHear(unittest.TestCase):
    # Test
    def test_AudioSWHear_witch(self):
        """Test AudioSWHear default value"""
        with AudioSWHear() as tape_recorder:
            tape_recorder.debug = True
            tape_recorder.__init__()
            self.assertEqual(22050, tape_recorder.rate)
            self.assertEqual(1, tape_recorder.channels)

    def test_AudioSWHear_default_value(self):
        """Test AudioSWHear default value"""
        tape_recorder = AudioSWHear()

        self.assertEqual(22050, tape_recorder.rate)
        self.assertEqual(1, tape_recorder.channels)

    def test_AudioSWHear_stream_read(self):
        """Test AudioSWHear stream_read"""
        if not os.environ.get("CI_JOB_ID"):
            tape_recorder = AudioSWHear()
            tape_recorder.debug = True
            tape_recorder.debug_level = 3
            tape_recorder.create_audio()
            tape_recorder.stream_create()
            tape_recorder.stream_start()
            self.assertEqual(tape_recorder.chunk_size, len(tape_recorder.stream_read()))
            tape_recorder.close_all()

    def test_AudioSWHear_stream_start(self):
        """Test AudioSWHear stream_start"""
        if not os.environ.get("CI_JOB_ID"):
            tape_recorder = AudioSWHear(start_streaming=False)
            tape_recorder.debug = True
            tape_recorder.debug_level = 3
            self.assertEqual(None, tape_recorder.stream)
            tape_recorder.create_audio()
            tape_recorder.stream_create()
            tape_recorder.stream_start()
            self.assertEqual(pyaudio.Stream, type(tape_recorder.stream))
            tape_recorder.close_all()

    def test_AudioSWHear_stream_stop(self):
        """Test AudioSWHear stream_stop"""
        if not os.environ.get("CI_JOB_ID"):
            tape_recorder = AudioSWHear(start_streaming=True)
            tape_recorder.debug = True
            tape_recorder.debug_level = 3
            tape_recorder.create_audio()
            tape_recorder.stream_create()
            tape_recorder.stream_start()
            self.assertEqual(pyaudio.Stream, type(tape_recorder.stream))
            self.assertTrue(tape_recorder.stream.is_active())
            tape_recorder.stream_stop()
            self.assertFalse(tape_recorder.stream.is_active())

    def test_AudioSWHear_tape_add(self):
        """Test AudioSWHear tape_add"""
        if not os.environ.get("CI_JOB_ID"):
            tape_recorder = AudioSWHear()
            tape_recorder.debug = True
            tape_recorder.debug_level = 3
            tape_recorder.create_audio()
            tape_recorder.stream_create()
            tape_recorder.stream_start()
            tape_recorder.tape_add()
            tape_recorder.close_all()

    def test_AudioSWHear_tape_flush(self):
        """Test AudioSWHear tape_slush"""
        if not os.environ.get("CI_JOB_ID"):
            tape_recorder = AudioSWHear()
            tape_recorder.debug = True
            tape_recorder.debug_level = 3
            tape_recorder.create_audio()
            tape_recorder.stream_create()
            tape_recorder.stream_start()
            tape_recorder.tape_flush()
            tape_recorder.close_all()


if __name__ == "__main__":
    unittest.main()
