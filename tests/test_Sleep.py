from glxaudio.Sleep import Sleep
import unittest


class TestSleep(unittest.TestCase):
    def setUp(self):
        self.sleep = Sleep()

    def test_duration_start(self):
        self.sleep.duration_start = 42
        self.assertEqual(self.sleep.duration_start, 42)

    def test_with(self):
        with Sleep() as sleep_object:
            sleep_object.verbose = True
            sleep_object.sleep(0.42)


if __name__ == "__main__":
    unittest.main()
