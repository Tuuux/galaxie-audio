#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

from glxaudio.AudioRecorder import AudioRecorder
from glxaudio.AudioUtils import get_format_to_dtype
from glxaudio.file import File

import wave
import unittest
from array import array


class TestAudioRercorder(unittest.TestCase):
    def setUp(self):
        self.recorder = AudioRecorder()

    def test_file(self):
        self.assertTrue(isinstance(self.recorder.file, File))

        file = File()
        self.recorder.file = file
        self.assertEqual(file, self.recorder.file)

        self.recorder.file = None
        self.assertNotEqual(file, self.recorder.file)
        self.assertTrue(isinstance(self.recorder.file, File))

        self.assertRaises(TypeError, setattr, self.recorder, 'file', 42)

    def test_AudioRecorder_with_capability(self):
        with AudioRecorder() as recorder:
            recorder.debug = True

    def test_AudioPLayer_set_duration_start(self):
        self.assertEqual(None, self.recorder.duration_start)

        self.recorder.set_duration_start()
        self.assertEqual(float, type(self.recorder.duration_start))

    def test_AudioRecorder_get_duration_start(self):
        self.assertEqual(None, self.recorder.get_duration_start())

        self.recorder.set_duration_start()
        self.assertEqual(float, type(self.recorder.get_duration_start()))

    def test_AudioRercorder_set_threshold(self):
        self.assertEqual(1638, int(self.recorder.threshold))

        self.recorder.threshold = 100
        self.assertEqual(32767.0, self.recorder.threshold)

        self.recorder.threshold = 0
        self.assertEqual(0, self.recorder.threshold)

        self.recorder.threshold = 10
        self.assertEqual(3276.7, self.recorder.threshold)

        self.assertRaises(TypeError, setattr, self.recorder, "threshold", "Hello.42")

    def test_AudioRercorder_set_threshold_db(self):
        self.assertEqual(1638, int(self.recorder.threshold))

        self.recorder.set_threshold_db(-20)
        self.assertEqual(21248, self.recorder.threshold)

        # test error
        self.assertRaises(TypeError, self.recorder.set_threshold_db, None)

    def test_AudioRercorder_get_timeout_length(self):
        self.assertEqual(23.4375, self.recorder.get_timeout_length())

        self.recorder.timeout_length = 78.125
        self.assertEqual(78.125, self.recorder.get_timeout_length())

    def test_AudioRercorder_set_timeout_length(self):
        self.assertEqual(23.4375, self.recorder.timeout_length)

        self.recorder.set_timeout_length(10)
        self.assertEqual(78.125, self.recorder.timeout_length)

        self.assertRaises(TypeError, self.recorder.set_timeout_length, None)

    def test_AudioRercorder_get_trim_to_append(self):
        self.assertEqual(self.recorder.rate / 10, self.recorder.get_trim_to_append())

        self.recorder.trim_to_append = self.recorder.rate / 100
        self.assertEqual(self.recorder.rate / 100, self.recorder.get_trim_to_append())

    def test_AudioRercorder_set_trim_to_append(self):
        self.assertEqual(self.recorder.rate / 10, self.recorder.trim_to_append)

        self.recorder.set_trim_to_append(self.recorder.rate / 100)
        self.assertEqual(self.recorder.rate / 100, self.recorder.trim_to_append)

        self.assertRaises(TypeError, self.recorder.set_trim_to_append, None)

    def test_AudioRercorder_is_silent(self):
        self.recorder.debug = True
        try:
            wave.open("tests/test01.wav")
            wav = "tests/test01.wav"
        except FileNotFoundError:
            wave.open("test01.wav")
            wav = "test01.wav"

        self.recorder.file.path = wav
        self.recorder.wave = wave.open(self.recorder.file.path)

        self.recorder.threshold = 0

        data = self.recorder.wave.readframes(self.recorder.chunk_size)

        while len(data) > 0:
            self.assertEqual(False, self.recorder.is_silent(data_chunk=data))
            data = self.recorder.wave.readframes(self.recorder.chunk_size)

        self.recorder.threshold = 100

        data = self.recorder.wave.readframes(self.recorder.chunk_size)

        while len(data) > 0:
            self.assertEqual(True, self.recorder.is_silent(data_chunk=data))
            data = self.recorder.wave.readframes(self.recorder.chunk_size)

        self.recorder.stream_close()

    def test_AudioRecorder_normalize_minus_dot_one_db(self):
        self.recorder.debug = True
        self.recorder.debug_level = 2

        data_all = array("h", [7, 3, 3, 2, 3, 6, 4, 4, 6, 5, 2, 5, 7, 3, 2, 5, 4, 1, 1, 4, -1, 4, 7467])

        self.assertEqual(7467, max(abs(i) for i in data_all))

        signal_to_quantization_noise_ratio = 6.02 * get_format_to_dtype(self.recorder.format)
        frame_db = self.recorder.get_base_ten_signed_max_value() / signal_to_quantization_noise_ratio
        frame_minusdb = int(frame_db * 0.1)

        value1 = self.recorder.get_base_ten_signed_max_value() - frame_minusdb
        value2 = max(abs(i) for i in self.recorder.normalize_minus_dot_one_db(data_all))
        self.assertEqual(value1, value2)

        # check error,
        self.assertRaises(TypeError, self.recorder.normalize_minus_dot_one_db, None, db=0)
        self.assertRaises(TypeError, self.recorder.normalize_minus_dot_one_db, data_all, db=None)

    def test_AudioRecorder_trim(self):
        self.recorder.debug = False

        # Basic test
        # recorder.channels = 1
        self.recorder.rate = 1
        self.recorder.threshold = 80

        data_all = array("h", [0, 1, 2, 3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3, 2, 1, 0])

        value_returned = self.recorder.trim(data_all, attack=0, release=0, release_knee=1.0)
        value_wanted = array("h", [0, 1, 2, 3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3, 2, 1, 0])

        self.assertEqual(value_wanted, value_returned)

        # Test attack
        self.recorder.threshold = 0.009
        value_returned = self.recorder.trim(data_all, attack=0, release=0, release_knee=1.0)
        # value_wanted = array("h", [84, 123, 0, 0, 0, 0, 0, 0, 122, 83])
        value_wanted = array("h", [3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3])
        self.assertEqual(value_wanted, value_returned)

        value_returned = self.recorder.trim(data_all, attack=1, release=0, release_knee=1.0)
        # value_wanted = array("h", [42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83])
        value_wanted = array("h", [2, 3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3])
        self.assertEqual(value_wanted, value_returned)

        value_returned = self.recorder.trim(data_all, attack=2, release=0, release_knee=1.0)
        # value_wanted = array("h", [3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83])
        value_wanted = array("h", [1, 2, 3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3])
        self.assertEqual(value_wanted, value_returned)

        value_returned = self.recorder.trim(data_all, attack=3, release=0, release_knee=1.0)
        # value_wanted = array("h", [2, 3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83])
        value_wanted = array("h", [0, 1, 2, 3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3])
        self.assertEqual(value_wanted, value_returned)

        # Test Release
        value_returned = self.recorder.trim(data_all, attack=0, release=0, release_knee=1.0)
        # value_wanted = array("h", [84, 123, 0, 0, 0, 0, 0, 0, 122, 83])
        value_wanted = array("h", [3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3])
        self.assertEqual(value_wanted, value_returned)

        value_returned = self.recorder.trim(data_all, attack=0, release=1, release_knee=1.0)
        # value_wanted = array("h", [84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41])
        value_wanted = array("h", [3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3, 2])
        self.assertEqual(value_wanted, value_returned)

        value_returned = self.recorder.trim(data_all, attack=0, release=2, release_knee=1.0)
        value_wanted = array("h", [3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3, 2, 1])
        self.assertEqual(value_wanted, value_returned)

        value_returned = self.recorder.trim(data_all, attack=0, release=3, release_knee=1.0)
        value_wanted = array("h", [3, 42, 84, 123, 0, 0, 0, 0, 0, 0, 122, 83, 41, 3, 2, 1, 0])
        self.assertEqual(value_wanted, value_returned)

        # check error,
        self.assertRaises(TypeError, self.recorder.trim, None, attack=0, release=3, release_knee=1.0)
        self.assertRaises(TypeError, self.recorder.trim, data_all, attack=None, release=3, release_knee=1.0)
        self.assertRaises(TypeError, self.recorder.trim, data_all, attack=0, release=None, release_knee=1.0)
        self.assertRaises(TypeError, self.recorder.trim, data_all, attack=0, release=3, release_knee=None)


if __name__ == "__main__":
    unittest.main()
