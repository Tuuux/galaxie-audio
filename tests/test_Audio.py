#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

import os
import unittest
import wave

import numpy
import pyaudio

from glxaudio.Audio import Audio
from glxaudio.AudioConstants import GLXAUDIO
from glxaudio.file import File


class TestAudio(unittest.TestCase):
    def setUp(self):
        self.audio = Audio()

    def test_Audio_get_new_audio(self):
        self.assertEqual(None, self.audio.pyaudio_instance)

        self.audio.create_audio()
        self.assertEqual(pyaudio.PyAudio, type(self.audio.pyaudio_instance))

        self.assertNotEqual(self.audio.pyaudio_instance, self.audio.create_audio())

    def test_pyaudio_instance(self):
        self.assertEqual(None, self.audio.pyaudio_instance)

        self.audio.create_audio()
        self.assertEqual(pyaudio.PyAudio, type(self.audio.pyaudio_instance))

        self.assertNotEqual(self.audio.pyaudio_instance, self.audio.create_audio())

    def test_format(self):
        # 32 bit float
        self.audio.format = GLXAUDIO.FORMAT_FLOAT32
        self.assertEqual(self.audio.format, GLXAUDIO.FORMAT_FLOAT32)
        self.assertEqual(self.audio.dtype, numpy.float32)
        self.assertEqual(4, self.audio.sample_width)

        # 32 bit int
        self.audio.format = GLXAUDIO.FORMAT_INT32
        self.assertEqual(self.audio.format, GLXAUDIO.FORMAT_INT32)
        self.assertEqual(self.audio.dtype, numpy.int32)
        self.assertEqual(4, self.audio.sample_width)

        # 24 bit int
        self.audio.format = GLXAUDIO.FORMAT_INT24
        self.assertEqual(self.audio.format, GLXAUDIO.FORMAT_INT24)
        self.assertEqual(self.audio.dtype, numpy.int32)
        self.assertEqual(3, self.audio.sample_width)

        # 16 bit int
        self.audio.format = GLXAUDIO.FORMAT_INT16
        self.assertEqual(self.audio.format, GLXAUDIO.FORMAT_INT16)
        self.assertEqual(self.audio.dtype, numpy.int16)
        self.assertEqual(2, self.audio.sample_width)

        # 8 bit int
        # 16 bit int
        self.audio.format = GLXAUDIO.FORMAT_INT8
        self.assertEqual(self.audio.format, GLXAUDIO.FORMAT_INT8)
        self.assertEqual(self.audio.dtype, numpy.int8)
        self.assertEqual(1, self.audio.sample_width)

        # 8 bit unsigned int
        self.audio.format = GLXAUDIO.FORMAT_UINT8
        self.assertEqual(self.audio.format, GLXAUDIO.FORMAT_UINT8)
        self.assertEqual(self.audio.dtype, numpy.uint8)
        self.assertEqual(1, self.audio.sample_width)

        # a custom data format
        # self.audio.format = GLXAUDIO.FORMAT_CUSTOM
        # self.assertEqual(self.audio.format, GLXAUDIO.FORMAT_CUSTOM)
        # self.assertEqual(self.audio.dtype, numpy.int32)
        # self.assertEqual(4, self.audio.sample_width)

        # check error
        self.assertRaises(TypeError, setattr, self.audio, "format", 42)

    def test_sample_width(self):
        self.assertEqual(2, self.audio.sample_width)

        self.audio.sample_width = 1
        self.assertEqual(1, self.audio.sample_width)

        self.audio.sample_width = 2
        self.assertEqual(2, self.audio.sample_width)

        self.audio.sample_width = 3
        self.assertEqual(3, self.audio.sample_width)

        self.audio.sample_width = 4
        self.assertEqual(4, self.audio.sample_width)

        # test error
        self.assertRaises(TypeError, setattr, self.audio, "sample_width", 0)
        self.assertRaises(TypeError, setattr, self.audio, "sample_width", 5)

    def test_dtype(self):
        self.assertEqual(numpy.int16, self.audio.dtype)

        self.audio.dtype = numpy.int8
        self.assertEqual(numpy.int8, self.audio.dtype)

        # test error
        self.assertRaises(TypeError, setattr, self.audio, "dtype", 42)

    def test_channels(self):
        self.assertEqual(2, self.audio.channels)

        self.audio.channels = 1
        self.assertEqual(1, self.audio.channels)

        self.assertRaises(TypeError, setattr, self.audio, "channels", "Hello.42")

    def test_rate(self):
        self.assertEqual(44100, self.audio.rate)

        self.audio.rate = 22050
        self.assertEqual(22050, self.audio.rate)

        self.assertRaises(TypeError, setattr, self.audio, "rate", "Hello.42")

    def test_Audio_get_base_ten_signed_max_value(self):
        self.audio.base_ten_signed_max_value = 32767
        self.assertEqual(32767, self.audio.get_base_ten_signed_max_value())

    def test_Audio_set_base_ten_signed_max_value(self):
        self.audio.set_base_ten_signed_max_value(32767)
        self.assertEqual(32767, self.audio.base_ten_signed_max_value)

        # test error
        self.assertRaises(TypeError, self.audio.set_base_ten_signed_max_value, None)

    def test_Audio_get_base_ten_signed_min_value(self):
        self.assertEqual(-32768, self.audio.get_base_ten_signed_min_value())

        self.audio.base_ten_signed_min_value = 32767
        self.assertEqual(32767, self.audio.get_base_ten_signed_min_value())

    def test_Audio_set_base_ten_signed_min_value(self):
        self.assertEqual(-32768, self.audio.base_ten_signed_min_value)

        self.audio.set_base_ten_signed_min_value(32767)
        self.assertEqual(32767, self.audio.base_ten_signed_min_value)

        # test error
        self.assertRaises(TypeError, self.audio.set_base_ten_signed_min_value, None)

    def test_chunk_size(self):
        self.assertEqual(1024, self.audio.chunk_size)

        self.audio.chunk_size = 2048
        self.assertEqual(2048, self.audio.chunk_size)

        # test error
        self.assertRaises(TypeError, setattr, self.audio, "chunk_size", "Hello.42")

    def test_stream(self):
        if not os.environ.get("CI_JOB_ID"):
            self.audio.format = GLXAUDIO.FORMAT_INT16
            self.audio.channels = 1
            self.audio.rate = 8000
            self.audio.debug = True
            self.audio.debug_level = 2

            self.assertEqual(None, self.audio.stream)

            self.audio.stream = self.audio.create_audio().open(
                format=self.audio.format, channels=self.audio.channels, rate=self.audio.rate, output=True
            )

            self.assertEqual(pyaudio.Stream, type(self.audio.stream))
            self.audio.stream_close()

            # test error
            # self.assertRaises(TypeError, self.audio.stream, None)

    def test_wave(self):
        try:
            wav = wave.open("tests/test01.wav")
        except FileNotFoundError:
            wav = wave.open("test01.wav")

        self.assertEqual(None, self.audio.wave)

        self.audio.wave = wav
        self.assertEqual(wav, self.audio.wave)

        self.audio.wave = None
        self.assertEqual(None, self.audio.wave)

        self.assertRaises(TypeError, setattr, self.audio, 'wave', 42)

    def test_close_stream(self):
        if not os.environ.get("CI_JOB_ID"):
            self.audio.verbose = True
            self.audio.debug = True
            self.audio.debug_level = 3
            self.audio.stream = self.audio.create_audio().open(format=GLXAUDIO.FORMAT_INT16, channels=1, rate=8000, output=True)

            self.audio.stream_close()
            self.audio.close_pyaudio()
            self.audio.close_all()

    def test_Audio_close_wave(self):
        self.audio.verbose = True
        self.audio.debug = True
        self.audio.debug_level = 3

        try:
            wave.open("tests/test02.wav")
            wav = "tests/test02.wav"
        except FileNotFoundError:
            wave.open("test02.wav")
            wav = "test02.wav"
        self.audio.wave = wave.open(wav)
        self.audio.close_wave()

    def test_Audio_close_all(self):
        if not os.environ.get("CI_JOB_ID"):
            self.audio.verbose = True
            self.audio.debug = True
            self.audio.debug_level = 3

            self.audio.stream = self.audio.create_audio().open(format=GLXAUDIO.FORMAT_INT16, channels=1, rate=8000, output=True)

            try:
                wave.open("tests/test02.wav")
                wav = "tests/test02.wav"
            except FileNotFoundError:
                wave.open("test02.wav")
                wav = "test02.wav"

            self.audio.wave = wave.open(wav)

            self.audio.close_all()

    def test_Audio_get_gain_db(self):
        self.audio.format = GLXAUDIO.FORMAT_INT16
        data_chunk = numpy.array(
            [1000, 2000, 3000, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 300], dtype=self.audio.dtype
        )
        self.assertEqual(-35.53, self.audio.get_db_from_chunk(data_chunk))

        data_chunk = numpy.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=self.audio.dtype)
        self.assertEqual(-90.31, self.audio.get_db_from_chunk(data_chunk))

        self.audio.close_all()

    def test_Audio_get_average_intensities_from_chunk(self):
        self.audio.format = GLXAUDIO.FORMAT_INT16
        data_chunk = numpy.array(
            [1000, 2000, 3000, 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 300], dtype=self.audio.dtype
        )
        self.assertEqual(23, self.audio.get_average_intensities_from_chunk(data_chunk, self.audio.sample_width))
        data_chunk = numpy.array([37000, 10, 37000, 10, 37000, 10, 37000, 10, 37000, 10], dtype=self.audio.dtype)
        self.assertEqual(119, self.audio.get_average_intensities_from_chunk(data_chunk, self.audio.sample_width))

        self.audio.close_all()


if __name__ == "__main__":
    unittest.main()
