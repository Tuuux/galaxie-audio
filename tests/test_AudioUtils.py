#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Radio Team, all rights reserved

from glxaudio.AudioUtils import get_quantification
from glxaudio.AudioUtils import get_format_to_dtype
from glxaudio.AudioUtils import get_format_to_human_view
from glxaudio.AudioUtils import sec2time
from glxaudio.AudioUtils import clamp
from glxaudio.AudioUtils import linear_to_db
from glxaudio.AudioUtils import from_percent
from glxaudio.AudioUtils import to_percent
from glxaudio.AudioUtils import percent_to_linear
from glxaudio.AudioUtils import get_scale
from glxaudio.AudioUtils import rms_flat
from glxaudio.AudioUtils import db_to_linear
from glxaudio.AudioUtils import linear_to_percent
from glxaudio.AudioConstants import GLXAUDIO

import unittest
from numpy import sqrt, mean, absolute


# Unittest
class TestAudioUtils(unittest.TestCase):
    def test_rms_flat(self):
        self.assertEqual(sqrt(mean(absolute(42) ** 2)), rms_flat(42))

    def test_db_to_linear(self):
        self.assertEqual(10 ** (42 / 20.0), db_to_linear(42))

    def test_linear_to_percent(self):
        self.assertEqual(4224 - (4224 * (42 / 100)), linear_to_percent(value=42, maximum=4224))

    def test_AudioUtils_linear_to_db(self):
        """Test AudioUtils.linear_to_db"""
        # Inspired by: https://en.wikipedia.org/wiki/Audio_bit_depth
        # self.assertTrue(isneginf(linear_to_db(0)))
        # 4 bits
        self.assertEqual(24.082399653118497, linear_to_db(16))
        # 8 bits
        self.assertEqual(48.164799306236993, linear_to_db(256))
        # 11 bits
        self.assertEqual(66.226599046075862, linear_to_db(2048))
        # 12 bits
        self.assertEqual(72.247198959355487, linear_to_db(4096))
        # 16 bits
        self.assertEqual(96.329598612473987, linear_to_db(65536))
        # 20 bits
        self.assertEqual(120.41199826559249, linear_to_db(1048576))
        # 24 bits
        self.assertEqual(144.49439791871097, linear_to_db(16777216))
        # 32 bits
        self.assertEqual(192.65919722494797, linear_to_db(4294967296))
        # 48 bits
        self.assertEqual(288.98879583742195, linear_to_db(281474976710656))
        # 64 bits
        self.assertRaises(TypeError, linear_to_db, 18446744073709551616)

    def test_AudioUtils_db_to_linear(self):
        """Test AudioUtils.db_to_linear"""
        # 4 bits
        self.assertEqual(24.082399653118497, linear_to_db(16))
        # 8 bits
        self.assertEqual(48.164799306236993, linear_to_db(256))
        # 11 bits
        self.assertEqual(66.226599046075862, linear_to_db(2048))
        # 12 bits
        self.assertEqual(72.247198959355487, linear_to_db(4096))
        # 16 bits
        self.assertEqual(96.329598612473987, linear_to_db(65536))
        # 20 bits
        self.assertEqual(120.41199826559249, linear_to_db(1048576))
        # 24 bits
        self.assertEqual(144.49439791871097, linear_to_db(16777216))
        # 32 bits
        self.assertEqual(192.65919722494797, linear_to_db(4294967296))
        # 48 bits
        self.assertEqual(288.98879583742195, linear_to_db(281474976710656))
        # 64 bits
        self.assertRaises(TypeError, linear_to_db, 18446744073709551616)

    def test_AudioUtils_to_percent(self):
        """Test AudioUtils.db_to_linear"""
        # 4 bits
        self.assertEqual(100, to_percent(16, 16))
        self.assertEqual(50, to_percent(16 / 2, 16))
        self.assertEqual(0, to_percent(0, 16))
        # 8 bits
        self.assertEqual(100, to_percent(256, 256))
        self.assertEqual(50, to_percent(256 / 2, 256))
        self.assertEqual(0, to_percent(0, 256))
        # 11 bits
        self.assertEqual(100, to_percent(2048, 2048))
        self.assertEqual(50, to_percent(2048 / 2, 2048))
        self.assertEqual(0, to_percent(0, 2048))
        # 12 bits
        self.assertEqual(100, to_percent(4096, 4096))
        self.assertEqual(50, to_percent(4096 / 2, 4096))
        self.assertEqual(0, to_percent(0, 4096))
        # 16 bits
        self.assertEqual(100, to_percent(65536, 65536))
        self.assertEqual(50, to_percent(65536 / 2, 65536))
        self.assertEqual(0, to_percent(0, 65536))
        # 20 bits
        self.assertEqual(100, to_percent(1048576, 1048576))
        self.assertEqual(50, to_percent(1048576 / 2, 1048576))
        self.assertEqual(0, to_percent(0, 1048576))
        # 24 bits
        self.assertEqual(100, to_percent(16777216, 16777216))
        self.assertEqual(50, to_percent(16777216 / 2, 16777216))
        self.assertEqual(0, to_percent(0, 16777216))
        # 32 bits
        self.assertEqual(100, to_percent(4294967296, 4294967296))
        self.assertEqual(50, to_percent(4294967296 / 2, 4294967296))
        self.assertEqual(0, to_percent(0, 4294967296))
        # 48 bits
        self.assertEqual(100, to_percent(281474976710656, 281474976710656))
        self.assertEqual(50, to_percent(281474976710656 / 2, 281474976710656))
        self.assertEqual(0, to_percent(0, 281474976710656))
        # 64 bits
        self.assertEqual(100, to_percent(18446744073709551616, 18446744073709551616))
        self.assertEqual(50, to_percent(18446744073709551616 / 2, 18446744073709551616))
        self.assertEqual(0, to_percent(0, 18446744073709551616))

    def test_AudioUtils_from_percent(self):
        """Test AudioUtils.db_to_linear"""
        list_t = [
            16,
            256,
            2048,
            4096,
            65536,
            1048576,
            16777216,
            4294967296,
            281474976710656,
            18446744073709551616
        ]
        for i in list_t:
            self.assertEqual(0, from_percent(0, i))
            self.assertEqual(i / 2, from_percent(50, i))
            self.assertEqual(0, from_percent(0, i))

    def test_AudioUtils_percent_to_linear(self):
        """Test AudioUtils.db_to_linear"""
        list_t = [
            16,
            256,
            2048,
            4096,
            65536,
            1048576,
            16777216,
            4294967296,
            281474976710656,
            18446744073709551616
        ]
        for i in list_t:
            self.assertEqual(i, percent_to_linear(0, i))
            self.assertEqual(i / 2, percent_to_linear(50, i))
            self.assertEqual(0, percent_to_linear(100, i))

    def test_Utils_get_quantification(self):
        """Test Utils.get_quantification()"""
        # Code in 8 bits = 2 puissance 8
        neg, pos, amplitude = get_quantification(8)
        self.assertEqual(neg, -128)
        self.assertEqual(pos, 127)
        self.assertEqual(amplitude, 48)

        # Code in 16 bits = 2 puissance 16
        neg, pos, amplitude = get_quantification(16)
        self.assertEqual(neg, -32768)
        self.assertEqual(pos, 32767)
        self.assertEqual(amplitude, 96)

        # Code in 20 bits = 2 puissance 20
        neg, pos, amplitude = get_quantification(20)
        self.assertEqual(neg, -524288)
        self.assertEqual(pos, 524287)
        self.assertEqual(amplitude, 120)

        # Code in 24 bits = 2 puissance 24
        # neg, pos, amplitude = get_quantification(24)
        # self.assertEqual(neg, -8388608)
        # self.assertEqual(pos, 8388607)
        # self.assertEqual(amplitude, 144)

        # Code in 32 bits = 2 puissance 32
        neg, pos, amplitude = get_quantification(32)
        self.assertEqual(neg, -2147483648)
        self.assertEqual(pos, 2147483647)
        self.assertEqual(amplitude, 192)

        # check error
        self.assertRaises(TypeError, get_quantification, None)

    def test_Utils_get_format_to_code(self):
        """Test AudioUtils.get_format_to_code()"""
        import pyaudio
        # 32 bit float
        # GLXAUDIO.FORMAT_FLOAT32
        self.assertEqual(GLXAUDIO.FORMAT_FLOAT32, pyaudio.paFloat32)
        self.assertEqual(32, get_format_to_dtype(GLXAUDIO.FORMAT_FLOAT32))

        # 32 bit int
        # GLXAUDIO.FORMAT_INT32
        self.assertEqual(GLXAUDIO.FORMAT_INT32, pyaudio.paInt32)
        self.assertEqual(32, get_format_to_dtype(GLXAUDIO.FORMAT_INT32))

        # 24 bit int
        # GLXAUDIO.FORMAT_INT24
        self.assertEqual(GLXAUDIO.FORMAT_INT24, pyaudio.paInt24)
        self.assertEqual(24, get_format_to_dtype(GLXAUDIO.FORMAT_INT24))

        # 16 bit int
        # GLXAUDIO.FORMAT_INT16
        self.assertEqual(GLXAUDIO.FORMAT_INT16, pyaudio.paInt16)
        self.assertEqual(16, get_format_to_dtype(GLXAUDIO.FORMAT_INT16))

        # 8 bit int
        # GLXAUDIO.FORMAT_INT8
        self.assertEqual(GLXAUDIO.FORMAT_INT8, pyaudio.paInt8)
        self.assertEqual(8, get_format_to_dtype(GLXAUDIO.FORMAT_INT8))

        # 8 bit unsigned int
        # GLXAUDIO.FORMAT_UINT8
        self.assertEqual(GLXAUDIO.FORMAT_UINT8, pyaudio.paUInt8)
        self.assertEqual(8, get_format_to_dtype(GLXAUDIO.FORMAT_UINT8))

        # a custom data format
        # GLXAUDIO.FORMAT_CUSTOM
        self.assertEqual(GLXAUDIO.FORMAT_CUSTOM, pyaudio.paCustomFormat)
        self.assertEqual(65536, get_format_to_dtype(GLXAUDIO.FORMAT_CUSTOM))

        # check error
        self.assertRaises(TypeError, get_format_to_dtype, None)

    def test_get_pa_sample_format_to_human_view(self):
        """Test get_format_to_human_view()"""
        # Exit as soon of possible
        # In case make the job
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_FLOAT32), '32 bit float')
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_INT32), '32 bit int')
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_INT24), '24 bit int')
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_INT16), '16 bit int')
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_INT8), '8 bit int')
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_UINT8), '8 bit unsigned int')
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_UINT8), '8 bit unsigned int')
        self.assertEqual(get_format_to_human_view(GLXAUDIO.FORMAT_CUSTOM), 'Custom')

        self.assertRaises(TypeError, get_format_to_human_view, None)

    def test_clamp(self):
        """Test Utils.clamp()"""
        self.assertEqual(42, clamp(value=1, smallest=42, largest=100))
        self.assertEqual(42.0, clamp(value=1.0, smallest=42, largest=100))
        self.assertEqual(1.0, clamp(value=1.0, smallest=-42, largest=100))

        self.assertEqual(42, clamp(value=100, smallest=0, largest=42))
        self.assertEqual(42.0, clamp(value=100.0, smallest=0, largest=42))

        self.assertRaises(TypeError, clamp, value=str(''), smallest=0, largest=42)
        self.assertRaises(TypeError, clamp, value=100, smallest=str(''), largest=42)
        self.assertRaises(TypeError, clamp, value=100, smallest=0, largest=str(''))

    def test_sec2time(self):
        """Test sec2time()"""
        self.assertEqual(sec2time(10, 3), '00:00:10.000')
        self.assertEqual(sec2time(1234567.8910, 0), '14 days, 06:56:07')
        self.assertEqual(sec2time(1234567.8910, 4), '14 days, 06:56:07.8910')
        self.assertEqual(sec2time([12, 345678.9], 3), ['00:00:12.000', '4 days, 00:01:18.900'])

    def test_get_scale(self):
        self.assertEqual(
            '[  0.          33.33333333  66.66666667 100.        ]',
            str(get_scale(minimal=0, maximum=100, division=4))
        )


if __name__ == '__main__':
    unittest.main()
