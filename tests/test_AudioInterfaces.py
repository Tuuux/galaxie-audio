import unittest
from glxaudio.AudioInterfaces import AudioInterfaces


class MyTestCase(unittest.TestCase):
    def test_print_interfaces(self):
        audiointerface = AudioInterfaces()
        audiointerface.print_interfaces()


if __name__ == '__main__':
    unittest.main()
