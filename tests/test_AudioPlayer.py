#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie Audio Team, all rights reserved

from glxaudio.AudioPlayer import AudioPLayer
from glxaudio.file import File
import wave
import unittest
import os
import time


class TestAudioPlayer(unittest.TestCase):
    def setUp(self):
        self.player = AudioPLayer()

    def test_detached(self):
        self.assertTrue(self.player.detached)

        self.player.detached = False
        self.assertFalse(self.player.detached)

        self.player.detached = None
        self.assertTrue(self.player.detached)

        self.assertRaises(TypeError, setattr, self.player, 'detached', 42)

    def test_duration_start(self):
        player = AudioPLayer()

        self.assertEqual(None, player.duration_start)

        player.duration_start = time.time()
        self.assertEqual(float, type(player.duration_start))

    def test_file(self):
        self.assertTrue(isinstance(self.player.file, File))

        file = File()
        self.player.file = file
        self.assertEqual(file, self.player.file)

        self.player.file = None
        self.assertNotEqual(file, self.player.file)
        self.assertTrue(isinstance(self.player.file, File))

        self.assertRaises(TypeError, setattr, self.player, 'file', 42)

    def test_play(self):
        try:
            wave.open("tests/test02.wav")
            wav = "tests/test02.wav"
        except FileNotFoundError:
            wave.open("test02.wav")
            wav = "test02.wav"

        # Start bar as a process
        with AudioPLayer() as player:
            player.debug = True
            player.debug_level = 3
            player.detached = False
            player.file.path = wav
            if not os.environ.get("CI_JOB_ID"):
                player.play()

    def test_AudioPlayer_callback(self):
        self.player.debug = True
        self.player.debug_level = 2

        try:
            wave.open("tests/test02.wav")
            wav = "tests/test02.wav"
        except FileNotFoundError:
            wave.open("test02.wav")
            wav = "test02.wav"

        self.player.file.path = wav
        self.player.wave = wave.open(self.player.file.path)
        self.player.duration_start = time.time()

        returned_value = self.player.callback(False, self.player.chunk_size, False, False)

        self.assertEqual(2, len(returned_value))
        self.assertEqual(tuple, type(returned_value))

    def test_AudioPlayer_with(self):
        try:
            wave.open("tests/test02.wav")
            wav = "tests/test02.wav"
        except FileNotFoundError:
            wave.open("test02.wav")
            wav = "test02.wav"

        with AudioPLayer() as player:
            player.debug = True
            player.debug_level = 2
            player.detached = False
            player.file.path = wav
            if not os.environ.get("CI_JOB_ID"):
                player.play()


if __name__ == "__main__":
    unittest.main()
