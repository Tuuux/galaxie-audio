-------------
GALAXIE AUDIO
-------------
CHANGELOG
---------
**0.1.5 - 20220102**
  * Add File Class
  * Migrate set / get method's to simple property
  * Add tests
  * Better Makefile
  * Fixe PLayer detach mode
**0.1.4 - 20211115**
  * Optimize CI/CD
  * Migrate **setuptools**
  * Delete ``docs/build`` when use ``make clean``
**0.1.3 - 20211112**
  * Add ``galaxie-viewer`` 0.5.0 support
**0.1.2 - 20211112**
  * Better Makefile
  * Fixe Audio Spectrum
**0.1.1 - 20210205**
  * New series ready to big thing
  * Use setup.py only
  * Add LICENSE.rst
  * Add Changelogs file
  * Better CI
  * rename GLXAudio module to glxaudio
**0.1 - 20210126**
  * Fixe CI troubles
  * CI use only Makefile
